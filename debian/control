Source: golang-google-cloud
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Martín Ferrari <tincho@debian.org>,
           Michael Stapelberg <stapelberg@debian.org>,
           Tim Potter <tpot@hpe.com>,
           Anthony Fok <foka@debian.org>,
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang (>= 1.31~),
               golang-any,
               golang-github-golang-protobuf-1-5-dev,
               golang-github-google-go-cmp-dev,
               golang-github-google-martian-dev,
               golang-github-googleapis-gax-go-dev,
               golang-github-googleapis-enterprise-certificate-proxy-dev,
               golang-go.opencensus-dev,
               golang-opentelemetry-otel-dev,
               golang-golang-x-oauth2-dev,
               golang-google-api-dev,
               golang-google-genproto-dev,
               golang-google-grpc-dev (>= 1.60.1),
               golang-google-protobuf-dev,
               golang-github-google-s2a-go-dev,
               golang-golang-x-tools-dev,
               golang-github-go-git-go-git-dev,
               golang-github-google-go-github-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-google-cloud
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-google-cloud.git
Homepage: https://cloud.google.com/go/home
Rules-Requires-Root: no
XS-Go-Import-Path: cloud.google.com/go

Package: golang-google-cloud-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-golang-protobuf-1-5-dev,
         golang-github-google-go-cmp-dev,
         golang-github-google-martian-dev,
         golang-github-googleapis-gax-go-dev,
         golang-github-googleapis-enterprise-certificate-proxy-dev,
         golang-go.opencensus-dev,
         golang-opentelemetry-otel-dev,
         golang-golang-x-oauth2-dev,
         golang-google-api-dev,
         golang-google-genproto-dev,
         golang-google-grpc-dev,
         golang-google-protobuf-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Google Cloud Platform APIs
 Package cloud contains Google Cloud Platform APIs related types and common
 functions.
 .
 Provides both the old google.golang.org/cloud and the new cloud.google.com/go
 trees.

Package: golang-google-cloud-compute-metadata-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-golang-x-net-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Google Cloud Platform APIs (only cloud/compute/metadata)
 This package contains only google.golang.org/cloud/compute/metadata, which is
 required by other packages (such as golang-golang-x-oauth2-dev), hence useful
 to split out to break circular dependencies.
 .
 Provides both the old google.golang.org/cloud and the new cloud.google.com/go
 trees.
