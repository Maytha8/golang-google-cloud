module google.golang.org/genproto/googleapis/api/servicecontrol

go 1.19

require (
	cloud.google.com/go/servicecontrol v1.12.4
	google.golang.org/genproto v0.0.0-20231120223509-83a465c0220f
	google.golang.org/genproto/googleapis/api v0.0.0-20231120223509-83a465c0220f
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231120223509-83a465c0220f
	google.golang.org/grpc v1.59.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
